package br.com.wirecard.challenge.features.orders

import br.com.wirecard.challenge.model.StoreModel

class OrdersActivityPresenterImpl(private var store: StoreModel) : OrdersActivityPresenter {

    override fun logout() {
        store.clearSessionData()
    }
}