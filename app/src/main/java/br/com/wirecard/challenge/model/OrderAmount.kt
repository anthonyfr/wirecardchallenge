package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OrderAmount(@SerializedName("total") val total: Int? = null,
                       @SerializedName("currency") val currency: String? = null,
                       @SerializedName("fees") val fees: Int? = null,
                       @SerializedName("liquid") val liquid: Int? = null): Serializable