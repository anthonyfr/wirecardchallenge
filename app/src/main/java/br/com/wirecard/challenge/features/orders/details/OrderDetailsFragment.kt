package br.com.wirecard.challenge.features.orders.details

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.wirecard.challenge.App
import br.com.wirecard.challenge.R
import br.com.wirecard.challenge.components.ErrorDialog
import br.com.wirecard.challenge.components.FormatterHelper
import br.com.wirecard.challenge.features.orders.OrdersActivity
import br.com.wirecard.challenge.features.orders.details.di.DaggerOrderDetailsComponent
import br.com.wirecard.challenge.features.orders.details.di.OrderDetailsModule
import br.com.wirecard.challenge.model.Order
import kotlinx.android.synthetic.main.fragment_order_detail.*
import javax.inject.Inject

class OrderDetailsFragment : Fragment(), OrderDetailsView {

    @Inject
    lateinit var presenter: OrderDetailsPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_order_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? OrdersActivity)?.showBackToolbar()

        DaggerOrderDetailsComponent.builder()
            .appComponent(App.appComponent)
            .orderDetailsModule(OrderDetailsModule(this))
            .build().inject(this)

        presenter.onReceiveId(arguments?.get(ORDER_ID))
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::presenter.isInitialized) {
            presenter.onDestroy()
        }
    }

    /**
     * Sets the summary section data
     */
    override fun setSummary(id: String, ownId: String, total: Int, paymentType: String) {
        tvId?.text = id
        tvOwnId?.text = ownId
        tvValue?.text = FormatterHelper.currency(total, null)
        tvType?.text = FormatterHelper.paymentType(paymentType)
    }

    /**
     * Sets the customer section data
     */
    override fun setCustomer(fullname: String, email: String) {
        tvCustomerName?.text = fullname
        tvCustomerEmail?.text = email
    }

    /**
     * Sets the status section data
     */
    override fun setStatus(created: String, updated: String, status: String) {
        tvDate?.text = FormatterHelper.timestamp(created)
        tvStatusDate?.text = FormatterHelper.timestamp(updated)
        tvStatus?.text = context?.getString(FormatterHelper.status(status))
        tvStatus.setTextColor(context?.resources?.getColor(FormatterHelper.statusColor(status))!!)
    }

    /**
     * Sets the finance section data
     */
    override fun setFinance(liquid: Int, fees: Int, total: Int) {
        tvTotalValue?.text = String.format("= %s", FormatterHelper.currency(liquid, null))
        tvTaxValue?.text = String.format("- %s", FormatterHelper.currency(fees, null))
        tvAmountValue?.text = String.format("+ %s", FormatterHelper.currency(total, null))
    }

    override fun setPayment(payments: Int) {
        tvPayments?.text = String.format(if (payments > 1) "%d pagamentos" else "%d pagamento", payments)
    }

    /**
     * Handles the errors on orders details loading
     *
     * @param throwable raised
     */
    override fun onError(throwable: Throwable?, orderId: String) {
        activity?.let {
            ErrorDialog.create(it)
                .throwable(throwable)
                .retryAction { presenter.loadOrderData(orderId) }
                .action { presenter.onRequestFailure() }
                .show()
        }
    }

    /**
     * Redirects to login activity
     */
    override fun redirectToLogin() {
        (activity as? OrdersActivity)?.redirectToLogin()
    }


    /**
     * Displays the loading
     */
    override fun showLoading() {
        loading?.visibility = View.VISIBLE
        summary?.visibility = View.GONE
        customer?.visibility = View.GONE
        customer_separator?.visibility = View.GONE
        date?.visibility = View.GONE
        separator?.visibility = View.GONE
        status?.visibility = View.GONE
        date_separator?.visibility = View.GONE
        finance?.visibility = View.GONE
        finance_separator?.visibility = View.GONE
        payment?.visibility = View.GONE
    }

    /**
     * Hides the loading
     */
    override fun hideLoading() {
        loading?.visibility = View.GONE
        summary?.visibility = View.VISIBLE
        customer?.visibility = View.VISIBLE
        customer_separator?.visibility = View.VISIBLE
        date?.visibility = View.VISIBLE
        separator?.visibility = View.VISIBLE
        status?.visibility = View.VISIBLE
        date_separator?.visibility = View.VISIBLE
        finance?.visibility = View.VISIBLE
        finance_separator?.visibility = View.VISIBLE
        payment?.visibility = View.VISIBLE
    }

    companion object {
        const val ORDER_ID = "ORDER_ID"

        fun newInstance(order: Order) : OrderDetailsFragment {
            val bundle = Bundle()
            bundle.putSerializable(ORDER_ID, order.id)

            val fragment = OrderDetailsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
