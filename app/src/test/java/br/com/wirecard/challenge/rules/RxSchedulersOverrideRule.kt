package br.com.wirecard.challenge.rules

import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import rx.Scheduler
import rx.android.plugins.RxAndroidPlugins
import rx.android.plugins.RxAndroidSchedulersHook
import rx.plugins.RxJavaHooks
import rx.schedulers.Schedulers

class RxSchedulersOverrideRule: TestRule {

    override fun apply(base: Statement?, description: Description?): Statement {
        return object : Statement() {
            override fun evaluate() {
                val rxAndroidPlugins = RxAndroidPlugins.getInstance()

                rxAndroidPlugins.reset()
                RxJavaHooks.reset()

                rxAndroidPlugins.registerSchedulersHook(TrampolineSchedulerHook())
                RxJavaHooks.setOnIOScheduler { Schedulers.trampoline() }
                RxJavaHooks.setOnNewThreadScheduler { Schedulers.trampoline() }

                base?.evaluate()

                rxAndroidPlugins.reset()
                RxJavaHooks.reset()
            }
        }
    }

    private class TrampolineSchedulerHook: RxAndroidSchedulersHook() {
        override fun getMainThreadScheduler(): Scheduler = Schedulers.trampoline()
    }
}