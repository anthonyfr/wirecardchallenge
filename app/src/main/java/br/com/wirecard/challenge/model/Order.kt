package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Order(@SerializedName("id") val id: String? = null,
                 @SerializedName("status") val currentStatus: String? = null,
                 @SerializedName("ownId") val ownId: String? = null,
                 @SerializedName("amount") val amount: OrderAmount? = null,
                 @SerializedName("customer") val customer: Customer? = null,
                 @SerializedName("updatedAt") val statusTimestamp: String? = null,
                 @SerializedName("payments") val payments: List<Payment>? = null): Serializable
