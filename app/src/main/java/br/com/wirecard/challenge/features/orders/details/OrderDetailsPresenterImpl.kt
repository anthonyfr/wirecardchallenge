package br.com.wirecard.challenge.features.orders.details

import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.network.Api
import rx.android.schedulers.AndroidSchedulers
import rx.internal.util.SubscriptionList
import rx.schedulers.Schedulers

/**
 * Orders details class to implement business logic
 */
class OrderDetailsPresenterImpl(private var view: OrderDetailsView, private  var api: Api, private var store: StoreModel): OrderDetailsPresenter {

    private var subscriptionList = SubscriptionList()

    /**
     * Receives the id of the order
     */
    override fun onReceiveId(any: Any?) {
        when (any) {
            is String -> loadOrderData(any)
        }
    }

    /**
     * Loads order details data
     *
     * @param orderId order id
     */
    override fun loadOrderData(orderId: String) {
        if (store.isLoginDataStored()) {
            val accessToken = store.getAccessToken()
            subscriptionList.add(api.getOrder(String.format("OAuth %s", accessToken), orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe({ response ->
                    view.setSummary(response.id!!, response.ownId!!, response.amount?.total!!,
                        response.payments?.get(0)?.funding?.method!!)
                    view.setCustomer(response.customer?.fullname!!, response.customer.email!!)
                    view.setStatus(response.created!!, response.updated!!, response.status!!)
                    view.setFinance(response.amount.liquid!!, response.amount.fees!!, response.amount.total)
                    view.setPayment(response.payments.size)
                }, { throwable ->
                    view.onError(throwable, orderId)
                }))
        } else {
            view.redirectToLogin()
        }
    }

    /**
     * Unsubscribes the subscription list on destroying activity context to avoid
     * memory leak and crashes.
     */
    override fun onDestroy() {
        if (!subscriptionList.isUnsubscribed) {
            subscriptionList.unsubscribe()
        }
    }

    /**
     * When a system failure related to the server or an unauthorized request happens,
     * the session is cleared and user redirected to the login screen.
     */
    override fun onRequestFailure() {
        store.clearSessionData()
        view.redirectToLogin()
    }
}