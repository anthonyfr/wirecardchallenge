package br.com.wirecard.challenge.network

import br.com.wirecard.challenge.model.LoginResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Multipart
import retrofit2.http.POST
import rx.Observable

interface LoginApi {
    @FormUrlEncoded
    @POST("/oauth/token")
    fun login(@Field("client_id") id: String? = null,
              @Field("client_secret") secret: String? = null,
              @Field("grant_type") grantType: String? = null,
              @Field("username") username: String? = null,
              @Field("password") password: String? = null,
              @Field("device_id") deviceId: String? = null,
              @Field("scope") scope: String? = null): Observable<LoginResponse>
}