package br.com.wirecard.challenge.features.login

import br.com.wirecard.challenge.App
import br.com.wirecard.challenge.BuildConfig
import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.network.LoginApi
import rx.android.schedulers.AndroidSchedulers
import rx.internal.util.SubscriptionList
import rx.schedulers.Schedulers

/**
 * Login class to implement business logic
 */
class LoginPresenterImpl(private var view: LoginView, private  var api: LoginApi, private var store: StoreModel): LoginPresenter {

    private var subscriptionList = SubscriptionList()

    /**
     * Handles login data change
     *
     * @param username inserted
     * @param password inserted
     */
    override fun onLoginDataChanged(username: String, password: String) {
        if (username.isNotEmpty() && password.isNotEmpty()) {
            view.enableButton()
        } else {
            view.disableButton()
        }
    }

    /**
     * Unsubscribes the subscription list on destroying activity context to avoid
     * memory leak and crashes.
     */
    override fun onDestroy() {
        if (!subscriptionList.isUnsubscribed) {
            subscriptionList.unsubscribe()
        }
    }

    /**
     * Handles when submit data is clicked
     */
    override fun onSubmitClicked(username: String, password: String) {
        if (username.isNotEmpty() && password.isNotEmpty()) {
            subscriptionList.add(api.login(
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                GRANT_TYPE,
                username, password,
                App.deviceId, APP_SCOPE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe({ response ->
                    store.storeLoginData(response)
                    view.checkDataAndRedirect(store)
                }, { throwable: Throwable? ->
                    view.onError(throwable)
                }))
        }
    }

    companion object {
        const val GRANT_TYPE = "password"
        const val APP_SCOPE = "APP_ADMIN"
    }
}