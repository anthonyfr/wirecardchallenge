package br.com.wirecard.challenge.features.orders

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import br.com.wirecard.challenge.App
import br.com.wirecard.challenge.R
import br.com.wirecard.challenge.features.login.LoginActivity
import br.com.wirecard.challenge.features.orders.details.OrderDetailsFragment
import br.com.wirecard.challenge.features.orders.di.DaggerOrdersActivityComponent
import br.com.wirecard.challenge.features.orders.di.OrdersActivityModule
import br.com.wirecard.challenge.features.orders.list.OrdersFragment
import br.com.wirecard.challenge.features.orders.list.di.DaggerOrdersComponent
import br.com.wirecard.challenge.features.orders.list.di.OrdersModule
import br.com.wirecard.challenge.model.Order
import br.com.wirecard.challenge.model.StoreModel

import kotlinx.android.synthetic.main.activity_orders.*
import javax.inject.Inject

/**
 * Orders class to implement view and Android native code
 */
class OrdersActivity : AppCompatActivity() {

    @Inject
    lateinit var presenter: OrdersActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orders)
        toolbar.setTitle(R.string.orders_title)
        setSupportActionBar(toolbar)

        DaggerOrdersActivityComponent.builder()
            .appComponent(App.appComponent)
            .ordersActivityModule(OrdersActivityModule())
            .build().inject(this)

        toolbar?.setOnMenuItemClickListener {
            presenter.logout()
            redirectToLogin()
            true
        }
        inflateOrdersListFragment()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.orders_menu, menu)
        return true
    }

    fun hideBackToolbar() {
        toolbar?.navigationIcon = null
        toolbar?.setNavigationOnClickListener(null)
    }

    fun showBackToolbar() {
        toolbar?.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back)
        toolbar?.setNavigationOnClickListener {
            supportFragmentManager.popBackStack()
        }
    }

    /**
     * Redirects to login activity
     */
    fun redirectToLogin() {
        startActivity(Intent(this@OrdersActivity, LoginActivity::class.java))
        finish()
    }

    /**
     * Inflates the orders list fragment
     */
    fun inflateOrdersListFragment() {
        val fragment = OrdersFragment.newInstance()
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.left_in, R.anim.left_out)
            .add(R.id.fragment_container, fragment)
            .commitAllowingStateLoss()
    }

    /**
     * Inflates the orders list fragment
     */
    fun inflateOrderDetailsFragment(order: Order) {
        val fragment = OrderDetailsFragment.newInstance(order)
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.left_in, R.anim.left_out)
            .addToBackStack(fragment.javaClass.simpleName)
            .replace(R.id.fragment_container, fragment)
            .commitAllowingStateLoss()
    }
}
