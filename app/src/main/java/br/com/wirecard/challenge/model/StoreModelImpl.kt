package br.com.wirecard.challenge.model

import com.orhanobut.hawk.Hawk

/**
 * Helper to manage the storage of data
 */
class StoreModelImpl : StoreModel {
        /**
         * Stores login data encrypted
         *
         * @param data the data to be stored
         */
        override fun storeLoginData(data: LoginResponse) {
            Hawk.put(LoginResponse::accessToken.name, data.accessToken)
            Hawk.put(LoginIdentification::id.name, data.moipAccount.id)
        }

        /**
         * Checks if login data is stored
         */
        override fun isLoginDataStored(): Boolean {
            return Hawk.contains(LoginResponse::accessToken.name) &&
                    Hawk.contains(LoginIdentification::id.name)
        }

        /**
         * Retrieves the access token stored
         *
         * @return access token
         */
        override fun getAccessToken(): String {
            return Hawk.get(LoginResponse::accessToken.name)
        }

        /**
         * Clear data stored by app
         */
        override fun clearSessionData() {
            Hawk.deleteAll()
        }
}