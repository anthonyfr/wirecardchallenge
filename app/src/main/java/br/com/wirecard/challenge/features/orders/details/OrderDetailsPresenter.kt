package br.com.wirecard.challenge.features.orders.details

interface OrderDetailsPresenter {
    fun loadOrderData(orderId: String)
    fun onReceiveId(any: Any?)
    fun onRequestFailure()
    fun onDestroy()
}