package br.com.wirecard.challenge.features.login.di

import br.com.wirecard.challenge.di.ActivityScope
import br.com.wirecard.challenge.di.AppComponent
import br.com.wirecard.challenge.features.login.LoginActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [LoginModule::class])
interface LoginComponent {
    fun inject(loginActivity: LoginActivity)
}