package br.com.wirecard.challenge.features.orders.list

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import br.com.wirecard.challenge.App
import br.com.wirecard.challenge.R
import br.com.wirecard.challenge.components.ErrorDialog
import br.com.wirecard.challenge.components.FormatterHelper
import br.com.wirecard.challenge.features.orders.OrdersActivity
import br.com.wirecard.challenge.features.orders.list.di.DaggerOrdersComponent
import br.com.wirecard.challenge.features.orders.list.di.OrdersModule
import br.com.wirecard.challenge.model.Order
import br.com.wirecard.challenge.model.OrdersSummary
import kotlinx.android.synthetic.main.fragment_order_list.*

import javax.inject.Inject

class OrdersFragment : Fragment(), OrdersView {

    @Inject
    lateinit var presenter: OrdersPresenter

    private var adapter: OrdersAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_order_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        DaggerOrdersComponent.builder()
            .appComponent(App.appComponent)
            .ordersModule(OrdersModule(this))
            .build().inject(this)

        (activity as? OrdersActivity)?.hideBackToolbar()
        presenter.loadOrders()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::presenter.isInitialized) {
            presenter.onDestroy()
        }
    }

    /**
     * Loads summary from orders
     */
    override fun loadSummary(summary: OrdersSummary) {
        tvSummary?.text = String.format(getString(R.string.orders_summary), summary.count,
            FormatterHelper.currencyTruncated(summary.amount, null))
    }

    /**
     * Loads the orders list
     */
    override fun loadRecycler(orders: MutableList<Order>) {
        adapter = OrdersAdapter(activity!!, orders)
        adapter?.setOnItemClickListener(AdapterView.OnItemClickListener { _, view, _, _ ->
            run {
                onOrderClick(view.tag as? Order)
            }
        })
        rvOrders?.adapter = adapter
        rvOrders?.layoutManager = LinearLayoutManager(activity)
    }

    /**
     * Handles click on a specific order
     */
    private fun onOrderClick(order: Order?) {
        order?.let {
            (activity as? OrdersActivity)?.inflateOrderDetailsFragment(it)
        }
    }

    /**
     * Redirects to login activity
     */
    override fun redirectToLogin() {
        (activity as? OrdersActivity)?.redirectToLogin()
    }

    /**
     * Handles the errors on orders loading
     *
     * @param throwable raised
     */
    override fun onError(throwable: Throwable?) {
        activity?.let {
            ErrorDialog.create(it)
                .throwable(throwable)
                .retryAction { presenter.loadOrders() }
                .action { presenter.onRequestFailure() }
                .show()
        }
    }

    /**
     * Displays the loading
     */
    override fun showLoading() {
        loading?.visibility = View.VISIBLE
        rvOrders?.visibility = View.GONE
        summary?.visibility = View.GONE
    }

    /**
     * Hides the loading
     */
    override fun hideLoading() {
        loading?.visibility = View.GONE
        rvOrders?.visibility = View.VISIBLE
        summary?.visibility = View.VISIBLE
    }

    companion object {
        fun newInstance() : OrdersFragment {
            return OrdersFragment()
        }
    }
}
