package br.com.wirecard.challenge

import br.com.wirecard.challenge.features.orders.list.OrdersPresenter
import br.com.wirecard.challenge.features.orders.list.OrdersView
import br.com.wirecard.challenge.features.orders.list.OrdersPresenterImpl
import br.com.wirecard.challenge.model.*
import br.com.wirecard.challenge.network.Api
import br.com.wirecard.challenge.rules.RxSchedulersOverrideRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.orhanobut.hawk.Hawk
import com.orhanobut.hawk.HawkBuilder
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import rx.Observable
import java.lang.Exception

@RunWith(JUnit4::class)
class OrdersPresenterTest {
    @get:Rule
    val rxSchedulerRule = RxSchedulersOverrideRule()

    @Mock
    lateinit var view: OrdersView

    @Mock
    lateinit var api: Api

    @Mock
    lateinit var store: StoreModel

    private lateinit var presenter: OrdersPresenter

    @Before
    fun setUp() {
        view = mock()
        api = mock()
        store = mock()

        presenter = OrdersPresenterImpl(view, api, store)
    }

    @Test
    fun `check load orders`() {
        val accessToken = "1234567890"
        val order = Order("test")
        val response = OrdersResponse(OrdersSummary(), mutableListOf(order))
        whenever(api.getOrders(String.format("OAuth %s", accessToken))).thenReturn(Observable.just(response))
        whenever(store.isLoginDataStored()).thenReturn(true)
        whenever(store.getAccessToken()).thenReturn(accessToken)

        presenter.loadOrders()
        verify(view).loadRecycler(mutableListOf(order))
    }

    @Test
    fun `check load summary`() {
        val accessToken = "1234567890"
        val summary = OrdersSummary()
        val response = OrdersResponse(summary, mock())
        whenever(api.getOrders(String.format("OAuth %s", accessToken))).thenReturn(Observable.just(response))
        whenever(store.isLoginDataStored()).thenReturn(true)
        whenever(store.getAccessToken()).thenReturn(accessToken)

        presenter.loadOrders()
        verify(view).loadSummary(summary)
    }

    @Test
    fun `check load orders error`() {
        val accessToken = "1234567890"
        val error = Throwable()
        whenever(api.getOrders(String.format("OAuth %s", accessToken))).thenReturn(Observable.error(error))
        whenever(store.isLoginDataStored()).thenReturn(true)
        whenever(store.getAccessToken()).thenReturn(accessToken)

        presenter.loadOrders()
        verify(view).onError(error)
    }
}
