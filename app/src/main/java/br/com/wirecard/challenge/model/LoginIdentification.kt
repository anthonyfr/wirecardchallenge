package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LoginIdentification(@SerializedName("id") val id: String? = null): Serializable
