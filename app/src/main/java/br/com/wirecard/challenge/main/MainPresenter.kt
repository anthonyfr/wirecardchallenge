package br.com.wirecard.challenge.main

interface MainPresenter {
    fun isDataStored() : Boolean
}