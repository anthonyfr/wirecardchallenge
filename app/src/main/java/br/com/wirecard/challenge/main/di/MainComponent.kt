package br.com.wirecard.challenge.main.di

import br.com.wirecard.challenge.di.ActivityScope
import br.com.wirecard.challenge.di.AppComponent
import br.com.wirecard.challenge.main.MainActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [MainModule::class])
interface MainComponent {
    fun inject(mainActivity: MainActivity)
}