package br.com.wirecard.challenge.features.orders.list.di

import br.com.wirecard.challenge.features.orders.list.OrdersPresenter
import br.com.wirecard.challenge.features.orders.list.OrdersPresenterImpl
import br.com.wirecard.challenge.features.orders.list.OrdersView
import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.network.Api
import dagger.Module
import dagger.Provides

@Module
class OrdersModule(private var view: OrdersView) {

    @Provides
    fun provideView() : OrdersView {
        return this.view
    }

    @Provides
    fun providePresenter(api: Api, store: StoreModel) : OrdersPresenter {
        return OrdersPresenterImpl(this.view, api, store)
    }
}