package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Payment(@SerializedName("id") val id: String? = null,
                   @SerializedName("fundingInstrument") val funding: FundingInstrument? = null) : Serializable