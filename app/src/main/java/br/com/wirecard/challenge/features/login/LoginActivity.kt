package br.com.wirecard.challenge.features.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import br.com.wirecard.challenge.App
import br.com.wirecard.challenge.R

import br.com.wirecard.challenge.components.ErrorDialog
import br.com.wirecard.challenge.features.login.di.DaggerLoginComponent
import br.com.wirecard.challenge.features.login.di.LoginModule
import br.com.wirecard.challenge.features.orders.OrdersActivity
import br.com.wirecard.challenge.model.StoreModel
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

/**
 * Login class to implement view and Android native code
 */
class LoginActivity : AppCompatActivity(), LoginView {

    @Inject
    lateinit var presenter: LoginPresenter

    private val onTextChanged = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // Not allowed
        }

        override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
            presenter.onLoginDataChanged(etUsername?.text?.toString().orEmpty(),
                etPassword?.text?.toString().orEmpty())
        }

        override fun afterTextChanged(s: Editable?) {
            // Not allowed
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerLoginComponent.builder()
            .appComponent(App.appComponent)
            .loginModule(LoginModule(this))
            .build().inject(this)

        setContentView(R.layout.activity_login)
        setUpListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::presenter.isInitialized) {
            presenter.onDestroy()
        }
    }

    /**
     * Sets up listeners for edit texts changes
     */
    private fun setUpListeners() {
        etUsername?.addTextChangedListener(onTextChanged)
        etPassword?.addTextChangedListener(onTextChanged)
        btSubmit?.setOnClickListener { presenter.onSubmitClicked(etUsername?.text?.toString().orEmpty(),
            etPassword?.text?.toString().orEmpty())
        }
    }

    /**
     * Handles the errors on form submit
     *
     * @param throwable raised on submission
     */
    override fun onError(throwable: Throwable?) {
        ErrorDialog.create(this)
            .throwable(throwable)
            .retryAction { presenter.onSubmitClicked(etUsername?.text?.toString().orEmpty(),
                etPassword?.text?.toString().orEmpty()) }
            .show()
    }

    override fun checkDataAndRedirect(store: StoreModel) {
        if (store.isLoginDataStored()) {
            val intent = Intent(this@LoginActivity, OrdersActivity::class.java)
            this@LoginActivity.startActivity(intent)
        }
    }

    /**
     * Enables submit button
     */
    override fun enableButton() {
        btSubmit?.isEnabled = true
    }

    /**
     * Disables submit button
     */
    override fun disableButton() {
        btSubmit?.isEnabled = false
    }

    /**
     * Displays the loading
     */
    override fun showLoading() {
        btSubmit?.visibility = View.GONE
        loading?.visibility = View.VISIBLE
    }

    /**
     * Hides the loading
     */
    override fun hideLoading() {
        btSubmit?.visibility = View.VISIBLE
        loading?.visibility = View.GONE
    }
}