package br.com.wirecard.challenge.features.orders.details.di

import br.com.wirecard.challenge.di.ActivityScope
import br.com.wirecard.challenge.di.AppComponent
import br.com.wirecard.challenge.features.orders.details.OrderDetailsFragment
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [OrderDetailsModule::class])
interface OrderDetailsComponent {
    fun inject(orderDetailsFragment: OrderDetailsFragment)
}