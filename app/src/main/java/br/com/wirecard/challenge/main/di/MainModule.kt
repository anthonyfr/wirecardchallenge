package br.com.wirecard.challenge.main.di

import br.com.wirecard.challenge.main.MainPresenter
import br.com.wirecard.challenge.main.MainPresenterImpl
import br.com.wirecard.challenge.model.StoreModel
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    fun providePresenter(store: StoreModel) : MainPresenter {
        return MainPresenterImpl(store)
    }
}