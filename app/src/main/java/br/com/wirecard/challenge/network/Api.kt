package br.com.wirecard.challenge.network

import br.com.wirecard.challenge.model.OrderDetailsResponse
import br.com.wirecard.challenge.model.OrdersResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import rx.Observable

interface Api {
    @GET("orders")
    fun getOrders(@Header("Authorization") accessToken: String): Observable<OrdersResponse>

    @GET("orders/{orderId}")
    fun getOrder(@Header("Authorization") accessToken: String,
                 @Path("orderId") orderId: String): Observable<OrderDetailsResponse>
}