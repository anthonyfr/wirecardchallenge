package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Customer(@SerializedName("email") val email: String? = null,
                    @SerializedName("fullname") val fullname: String? = null): Serializable