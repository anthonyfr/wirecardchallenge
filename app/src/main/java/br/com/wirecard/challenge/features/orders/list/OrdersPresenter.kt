package br.com.wirecard.challenge.features.orders.list

interface OrdersPresenter {
    fun loadOrders()
    fun onRequestFailure()
    fun onDestroy()
}