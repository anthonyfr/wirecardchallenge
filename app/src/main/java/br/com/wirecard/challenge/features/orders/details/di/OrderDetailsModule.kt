package br.com.wirecard.challenge.features.orders.details.di

import br.com.wirecard.challenge.features.orders.details.OrderDetailsPresenter
import br.com.wirecard.challenge.features.orders.details.OrderDetailsPresenterImpl
import br.com.wirecard.challenge.features.orders.details.OrderDetailsView
import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.network.Api
import dagger.Module
import dagger.Provides

@Module
class OrderDetailsModule(private var view: OrderDetailsView) {

    @Provides
    fun provideView() : OrderDetailsView {
        return this.view
    }

    @Provides
    fun providePresenter(api: Api, store: StoreModel) : OrderDetailsPresenter {
        return OrderDetailsPresenterImpl(this.view, api, store)
    }
}