package br.com.wirecard.challenge.model

interface StoreModel {
    fun storeLoginData(data: LoginResponse)
    fun isLoginDataStored(): Boolean
    fun getAccessToken(): String
    fun clearSessionData()
}