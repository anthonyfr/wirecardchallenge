package br.com.wirecard.challenge

import android.annotation.SuppressLint
import android.provider.Settings
import android.support.multidex.MultiDexApplication
import br.com.wirecard.challenge.di.AppComponent
import br.com.wirecard.challenge.di.DaggerAppComponent
import com.orhanobut.hawk.Hawk

class App: MultiDexApplication() {

    @SuppressLint("HardwareIds")
    @Override
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent?.inject(this)

        deviceId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        Hawk.init(this).build()
    }

    companion object {
        @JvmStatic
        var deviceId: String = ""
        var appComponent: AppComponent? = null
    }
}