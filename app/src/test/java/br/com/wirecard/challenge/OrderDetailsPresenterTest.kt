package br.com.wirecard.challenge

import br.com.wirecard.challenge.features.orders.details.OrderDetailsPresenter
import br.com.wirecard.challenge.features.orders.details.OrderDetailsPresenterImpl
import br.com.wirecard.challenge.features.orders.details.OrderDetailsView
import br.com.wirecard.challenge.model.*
import br.com.wirecard.challenge.network.Api
import br.com.wirecard.challenge.rules.RxSchedulersOverrideRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import rx.Observable

@RunWith(JUnit4::class)
class OrderDetailsPresenterTest {
    @get:Rule
    val rxSchedulerRule = RxSchedulersOverrideRule()

    @Mock
    lateinit var view: OrderDetailsView

    @Mock
    lateinit var api: Api

    @Mock
    lateinit var store: StoreModel

    private lateinit var presenter: OrderDetailsPresenter

    @Before
    fun setUp() {
        view = mock()
        api = mock()
        store = mock()

        presenter = OrderDetailsPresenterImpl(view, api, store)
    }

    @Test
    fun `check load status order data`() {
        val accessToken = "1234567890"
        val order = Order("test")
        val response = OrderDetailsResponse(created = "2018-08-24T00:24:25-0300",
            updated = "2018-08-24T00:24:25-0300", status = "PAID",
            customer = Customer(fullname = "Joao", email="email@email.com"),
            amount = OrderAmount(total = 123, liquid = 120, fees = 3),
            id = "id", ownId = "ownId",
            payments = listOf(Payment(funding = FundingInstrument(method = "BOLETO")))
        )
        whenever(api.getOrder(String.format("OAuth %s", accessToken), order.id!!)).thenReturn(Observable.just(response))
        whenever(store.isLoginDataStored()).thenReturn(true)
        whenever(store.getAccessToken()).thenReturn(accessToken)

        presenter.loadOrderData(order.id!!)
        verify(view).setStatus(created = "2018-08-24T00:24:25-0300",
            updated = "2018-08-24T00:24:25-0300", status = "PAID")
        verify(view).setCustomer(fullname = "Joao", email="email@email.com")
        verify(view).setFinance(total = 123, liquid = 120, fees = 3)
        verify(view).setSummary(total = 123, id = "id", ownId = "ownId", paymentType = "BOLETO")
        verify(view).setPayment(payments = 1)
    }
}
