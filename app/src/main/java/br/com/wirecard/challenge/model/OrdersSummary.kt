package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OrdersSummary(@SerializedName("count") val count: Int? = null,
                         @SerializedName("amount") val amount: Int? = null): Serializable