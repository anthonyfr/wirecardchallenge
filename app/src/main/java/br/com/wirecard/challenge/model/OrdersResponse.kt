package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OrdersResponse(@SerializedName("summary") val summary: OrdersSummary,
                          @SerializedName("orders") val orders: MutableList<Order>): Serializable
