package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class FundingInstrument(@SerializedName("id") val id: String? = null,
                             @SerializedName("method") val method: String? = null) : Serializable