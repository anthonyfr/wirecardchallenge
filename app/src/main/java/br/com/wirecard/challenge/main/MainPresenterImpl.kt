package br.com.wirecard.challenge.main

import br.com.wirecard.challenge.model.StoreModel

class MainPresenterImpl(private var store: StoreModel) : MainPresenter {

    override fun isDataStored() : Boolean {
        return store.isLoginDataStored()
    }
}