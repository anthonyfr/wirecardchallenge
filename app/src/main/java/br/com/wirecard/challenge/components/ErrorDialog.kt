package br.com.wirecard.challenge.components

import android.app.AlertDialog
import android.content.Context
import br.com.wirecard.challenge.R
import br.com.wirecard.challenge.network.ConnectionHelper
import retrofit2.HttpException
import java.net.UnknownHostException

/**
 * Handles error dialogs
 */
class ErrorDialog {

    private lateinit var context: Context
    private var type: ERROR = ERROR.CUSTOM
    private var title: String? = null
    private var message: String? = null
    private var button: String? = null
    private var action: (() -> Unit)? = null
    private var retryAction: (() -> Unit)? = null

    /**
     * Receives the throwable and selects the error code
     */
    fun throwable(throwable: Throwable?) : ErrorDialog {
        this.type = when (throwable) {
            is HttpException -> {
                when (throwable.code()) {
                    500 -> ERROR.SERVICE
                    502, 503, 504 -> {
                        if (ConnectionHelper.isConnectedInternet(context)) {
                            ERROR.CONNECTION
                        } else {
                            ERROR.SYSTEM
                        }
                    }
                    400 -> ERROR.UNAUTHORIZED
                    else -> ERROR.SYSTEM
                }
            }
            is UnknownHostException -> ERROR.CONNECTION
            else -> {
                ERROR.SYSTEM
            }
        }

        return this
    }

    /**
     * Sets dialog custom title
     */
    fun title(title: Int) : ErrorDialog {
        this.title = context.getString(title)
        return this
    }

    /**
     * Sets dialog custom message
     */
    fun message(message: Int) : ErrorDialog {
        this.message = context.getString(message)
        return this
    }

    /**
     * Sets dialog custom button text
     */
    fun button(button: Int) : ErrorDialog {
        this.button = context.getString(button)
        return this
    }

    /**
     * Sets dialog custom action
     */
    fun action(action: () -> Unit) : ErrorDialog {
        this.action = action
        return this
    }

    /**
     * Sets dialog custom retry action
     */
    fun retryAction(action: () -> Unit) : ErrorDialog {
        this.retryAction = action
        return this
    }

    /**
     * Displays the dialog
     */
    fun show() {
        val dialog = AlertDialog.Builder(context, R.style.ModalTheme)

        if (this.type != ERROR.CUSTOM) {
            setupNetworkError()
        }

        dialog.setTitle(this.title)
            .setMessage(this.message)
            .setCancelable(false)
            .setPositiveButton(this.button) { _, _ ->  if (this.type == ERROR.CONNECTION)
                this.retryAction?.invoke() else this.action?.invoke() }
            .show()
    }

    /**
     * Sets up the network error resources
     */
    private fun setupNetworkError() {
        when (this.type) {
            ERROR.SERVICE -> {
                this.title = context.getString(R.string.error_service_title)
                this.message = context.getString(R.string.error_service_message)
                this.button = context.getString(R.string.error_service_button)
            }
            ERROR.CONNECTION -> {
                this.title = context.getString(R.string.error_connection_title)
                this.message = context.getString(R.string.error_connection_message)
                this.button = context.getString(R.string.error_connection_button)
            }
            ERROR.SYSTEM -> {
                this.title = context.getString(R.string.error_system_title)
                this.message = context.getString(R.string.error_system_message)
                this.button = context.getString(R.string.error_system_button)
            }
            ERROR.UNAUTHORIZED -> {
                this.title = context.getString(R.string.error_unauthorized_title)
                this.message = context.getString(R.string.error_unauthorized_message)
                this.button = context.getString(R.string.error_unauthorized_button)
            }
            else -> {}
        }
    }

    enum class ERROR {
        SERVICE, SYSTEM, CONNECTION, UNAUTHORIZED, CUSTOM
    }

    companion object {
        @JvmStatic
        fun create(context: Context) : ErrorDialog {
            val newInstance = ErrorDialog()
            newInstance.context = context
            return newInstance
        }
    }
}