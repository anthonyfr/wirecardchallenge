package br.com.wirecard.challenge.di

import android.app.Application
import br.com.wirecard.challenge.App
import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.network.Api
import br.com.wirecard.challenge.network.LoginApi
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, AppModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        fun build(): AppComponent
        @BindsInstance
        fun application(application: Application): Builder
    }

    fun inject(app: App)

    fun provideLoginApi(): LoginApi

    fun provideApi(): Api

    fun provideStoreModel(): StoreModel
}
