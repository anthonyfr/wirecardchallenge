package br.com.wirecard.challenge.features.orders.list

import br.com.wirecard.challenge.model.Order
import br.com.wirecard.challenge.model.OrdersSummary

interface OrdersView {
    fun loadSummary(summary: OrdersSummary)
    fun loadRecycler(orders: MutableList<Order>)
    fun showLoading()
    fun hideLoading()
    fun onError(throwable: Throwable?)
    fun redirectToLogin()
}