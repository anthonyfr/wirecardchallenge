package br.com.wirecard.challenge.components

import android.content.Context
import br.com.wirecard.challenge.R
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class FormatterHelper {
    companion object {
        @JvmStatic
        fun currency(value: Int?, currency: String?): String {
            return value?.let {
                val format = NumberFormat.getCurrencyInstance(Locale("pt", "BR"))
                format.currency = Currency.getInstance(currency ?: "BRL")
                format.format(it.toFloat().div(100))
            }.orEmpty()
        }

        @JvmStatic
        fun currencyTruncated(value: Int?, currency: String?): String {
            return value?.let {
                val format = NumberFormat.getCurrencyInstance(Locale("pt", "BR"))
                format.currency = Currency.getInstance(currency ?: "BRL")
                String.format("%s %dM", format.currency.getSymbol(Locale("pt", "BR")), it.div(100000))
            }.orEmpty()
        }

        @JvmStatic
        fun timestamp(timestamp: String?): String {
            try {
                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ", Locale("pt", "BR"))
                val date = format.parse(timestamp)
                return SimpleDateFormat("dd/MM/YYYY", Locale("pt", "BR")).format(date)
            } catch (e: ParseException) {
                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale("pt", "BR"))
                val date = format.parse(timestamp)
                return SimpleDateFormat("dd/MM/YYYY", Locale("pt", "BR")).format(date)
            }
        }

        @JvmStatic
        fun status(status: String?): Int {
            return status?.let {
                STATUS[it]
            } ?: 0
        }

        @JvmStatic
        fun statusColor(status: String?): Int {
            return status?.let {
                STATUS_COLOR[status]
            } ?: 0
        }

        @JvmStatic
        fun orderIcon(method: String?): Int {
            return method?.let {
                ICONS[it]
            } ?: 0
        }

        @JvmStatic
        fun paymentType(method: String?): String {
            return method?.let {
                PAYMENT_TYPES[it]
            }.orEmpty()
        }

        private val STATUS = mapOf(
            "REVERTED" to R.string.order_not_paid,
            "PAID" to R.string.order_paid,
            "WAITING" to R.string.order_waiting
        )

        private val STATUS_COLOR = mapOf(
            "REVERTED" to R.color.orderStatusReverted,
            "PAID" to R.color.orderStatusPaid,
            "WAITING" to R.color.orderStatusWaiting
        )

        private val ICONS = mapOf(
            "BOLETO" to R.drawable.barcode,
            "CREDIT_CARD" to R.drawable.card
        )

        private val PAYMENT_TYPES = mapOf(
            "BOLETO" to "Boleto",
            "CREDIT_CARD" to "Cartão de crédito"
        )
    }
}