package br.com.wirecard.challenge.features.orders.list.di

import br.com.wirecard.challenge.di.ActivityScope
import br.com.wirecard.challenge.di.AppComponent
import br.com.wirecard.challenge.features.orders.list.OrdersFragment
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [OrdersModule::class])
interface OrdersComponent {
    fun inject(ordersFragment: OrdersFragment)
}