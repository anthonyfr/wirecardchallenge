package br.com.wirecard.challenge.di

import android.util.Log
import br.com.wirecard.challenge.BuildConfig
import br.com.wirecard.challenge.network.Api
import br.com.wirecard.challenge.network.LoginApi
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApiModule {

    /**
     * Provide setting network api connection with 15 seconds time out.
     */
    @Provides
    @Singleton
    fun provideLoginApi(loggingInterceptor: HttpLoggingInterceptor): LoginApi {
        val client = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.LOGIN_URL)
            .client(client)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()

        return retrofit.create(LoginApi::class.java)
    }

    /**
     * Provide setting network login service connection with 15 seconds time out.
     */
    @Provides
    @Singleton
    fun provideApi(loggingInterceptor: HttpLoggingInterceptor): Api {
        val client = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(client)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()

        return retrofit.create(Api::class.java)
    }

    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor { message -> Log.d("ApiModule", message) }
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

}