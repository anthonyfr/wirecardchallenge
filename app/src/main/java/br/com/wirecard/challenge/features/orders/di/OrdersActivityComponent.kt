package br.com.wirecard.challenge.features.orders.di

import br.com.wirecard.challenge.di.ActivityScope
import br.com.wirecard.challenge.di.AppComponent
import br.com.wirecard.challenge.features.orders.OrdersActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [OrdersActivityModule::class])
interface OrdersActivityComponent {
    fun inject(ordersActivity: OrdersActivity)
}