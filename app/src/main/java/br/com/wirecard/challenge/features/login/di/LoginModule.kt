package br.com.wirecard.challenge.features.login.di

import br.com.wirecard.challenge.features.login.LoginPresenter
import br.com.wirecard.challenge.features.login.LoginPresenterImpl
import br.com.wirecard.challenge.features.login.LoginView
import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.network.LoginApi
import dagger.Module
import dagger.Provides

@Module
class LoginModule(private var view: LoginView) {

    @Provides
    fun provideView() : LoginView {
        return this.view
    }

    @Provides
    fun providePresenter(loginApi: LoginApi, store: StoreModel) : LoginPresenter {
        return LoginPresenterImpl(this.view, loginApi, store)
    }
}