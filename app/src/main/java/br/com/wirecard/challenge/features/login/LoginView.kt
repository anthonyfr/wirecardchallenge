package br.com.wirecard.challenge.features.login

import br.com.wirecard.challenge.model.StoreModel

/**
 * Interface for login view
 */
interface LoginView {
    fun enableButton()
    fun disableButton()
    fun showLoading()
    fun hideLoading()
    fun onError(throwable: Throwable?)
    fun checkDataAndRedirect(store: StoreModel)
}