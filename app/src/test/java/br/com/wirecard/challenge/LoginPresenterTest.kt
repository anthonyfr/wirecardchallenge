package br.com.wirecard.challenge

import br.com.wirecard.challenge.features.login.LoginPresenter
import br.com.wirecard.challenge.features.login.LoginPresenterImpl
import br.com.wirecard.challenge.features.login.LoginView
import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.network.LoginApi
import br.com.wirecard.challenge.rules.RxSchedulersOverrideRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import rx.Observable
import java.net.UnknownHostException

@RunWith(JUnit4::class)
class LoginPresenterTest {

    @get:Rule
    val rxSchedulerRule = RxSchedulersOverrideRule()

    @Mock
    lateinit var view: LoginView

    @Mock
    lateinit var api: LoginApi

    @Mock
    lateinit var store: StoreModel

    private lateinit var presenter: LoginPresenter

    @Before
    fun setUp() {
        view = mock()
        api = mock()
        store = mock()

        presenter = LoginPresenterImpl(view, api, store)
    }

    @Test
    fun `when login and password are filled submit button is enabled`() {
        presenter.onLoginDataChanged("test@test.com", "123456")

        verify(view).enableButton()
    }

    @Test
    fun `when password is cleared submit button is disabled`() {
        presenter.onLoginDataChanged("test@test.com", "")

        verify(view).disableButton()
    }

    @Test
    fun `when login is cleared submit button is disabled`() {
        presenter.onLoginDataChanged("", "123456")

        verify(view).disableButton()
    }

    @Test
    fun `when login is submitted and connection error is received error is called on view`() {
        val throwable: Throwable = UnknownHostException()

        whenever(api.login(BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            LoginPresenterImpl.GRANT_TYPE,
            "test@test.com", "123456",
            App.deviceId, LoginPresenterImpl.APP_SCOPE)
        ).thenReturn(Observable.error(throwable))

        presenter.onSubmitClicked("test@test.com", "123456")

        verify(view).onError(throwable)
    }
}