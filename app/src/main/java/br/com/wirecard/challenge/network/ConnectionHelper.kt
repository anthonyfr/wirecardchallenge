package br.com.wirecard.challenge.network

import android.content.Context
import android.net.ConnectivityManager

/**
 * Helper class to get network connection status
 */
class ConnectionHelper {
    companion object {

        /**
         * Checks if device is connected to the internet
         */
        @JvmStatic
        fun isConnectedInternet(context: Context): Boolean {
            return isWifi(context) || isMobile(context)
        }

        /**
         * Checks if device is connected to a wifi network on internet
         */
        @JvmStatic
        private fun isWifi(context: Context): Boolean {
            try {
                val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                return wifi != null && wifi.isConnectedOrConnecting
            } catch (ignored: Exception) {
                return false
            }
        }

        /**
         * Checks if device is connected to a mobile network on internet
         */
        @JvmStatic
        private fun isMobile(context: Context): Boolean {
            try {
                val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                return mobile != null && mobile.isConnectedOrConnecting
            } catch (ignored: Exception) {
                return false
            }

        }
    }
}