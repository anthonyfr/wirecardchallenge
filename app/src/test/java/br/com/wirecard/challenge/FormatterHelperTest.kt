package br.com.wirecard.challenge

import br.com.wirecard.challenge.components.FormatterHelper
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class FormatterHelperTest {

    @Test
    fun `format currency`() {
        val received = 15020
        assertEquals("R$ 150,20", FormatterHelper.currency(received, null))
    }

    @Test
    fun `format currency truncated`() {
        val received = 15122033
        assertEquals("R$ 151M", FormatterHelper.currencyTruncated(received, null))
    }

    @Test
    fun `format timestamp string to date`() {
        val received = "2018-08-24T00:24:25-0300"
        assertEquals("24/08/2018", FormatterHelper.timestamp(received))
    }

    @Test
    fun `format status`() {
        val received = "PAID"
        assertEquals(R.string.order_paid, FormatterHelper.status(received))
    }

    @Test
    fun `format status color`() {
        val received = "PAID"
        assertEquals(R.color.orderStatusPaid, FormatterHelper.statusColor(received))
    }

    @Test
    fun `format order icon`() {
        val received = "BOLETO"
        assertEquals(R.drawable.barcode, FormatterHelper.orderIcon(received))
    }
}
