package br.com.wirecard.challenge.features.orders.di

import br.com.wirecard.challenge.features.orders.OrdersActivityPresenter
import br.com.wirecard.challenge.features.orders.OrdersActivityPresenterImpl
import br.com.wirecard.challenge.model.StoreModel
import dagger.Module
import dagger.Provides

@Module
class OrdersActivityModule {

    @Provides
    fun providePresenter(store: StoreModel) : OrdersActivityPresenter {
        return OrdersActivityPresenterImpl(store)
    }
}