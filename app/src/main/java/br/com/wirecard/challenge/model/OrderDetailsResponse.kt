package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OrderDetailsResponse(@SerializedName("id") val id: String? = null,
                                @SerializedName("ownId") val ownId: String? = null,
                                @SerializedName("status") val status: String? = null,
                                @SerializedName("createdAt") val created: String? = null,
                                @SerializedName("updatedAt") val updated: String? = null,
                                @SerializedName("amount") val amount: OrderAmount? = null,
                                @SerializedName("payments") val payments: List<Payment>? = null,
                                @SerializedName("customer") val customer: Customer? = null) : Serializable
