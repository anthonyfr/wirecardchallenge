package br.com.wirecard.challenge.features.orders.list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import br.com.wirecard.challenge.R
import br.com.wirecard.challenge.components.FormatterHelper
import br.com.wirecard.challenge.model.Order
import kotlinx.android.synthetic.main.order_item.view.*


class OrdersAdapter(private val context: Context, private var data: MutableList<Order>) :
    RecyclerView.Adapter<OrdersAdapter.ViewHolder>() {

    private var onItemClickListener: AdapterView.OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.order_item, parent, false)
        return ViewHolder(view, this)
    }

    fun setOnItemClickListener(clickListener: AdapterView.OnItemClickListener) {
        onItemClickListener = clickListener
    }

    private fun onItemHolderClick(itemHolder: ViewHolder) {
        onItemClickListener?.onItemClick(null, itemHolder.itemView,
            itemHolder.adapterPosition, itemHolder.itemId)
    }

    private fun getItem(position: Int): Any? {
        return data.takeIf {
            itemCount < position
        }?.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repository = data[position]
        holder.itemView.tag = repository

        holder.tvValue.text = FormatterHelper.currency(repository.amount?.total, repository.amount?.currency)
        holder.tvEmail.text = repository.customer?.email
        holder.tvOwnId.text = repository.ownId
        holder.tvDate.text = FormatterHelper.timestamp(repository.statusTimestamp)
        holder.tvStatus.text = context.getString(FormatterHelper.status(repository.currentStatus))
        holder.tvStatus.setTextColor(context.resources.getColor(FormatterHelper.statusColor(repository.currentStatus)))

        if (!repository.payments.isNullOrEmpty()) {
            holder.ivIcon.setBackgroundResource(FormatterHelper.orderIcon(repository.payments[0].funding?.method))
            holder.ivIcon.contentDescription = when (repository.payments[0].funding?.method) {
                "BOLETO" -> context.getString(R.string.order_barcode)
                "CREDIT_CARD" -> context.getString(R.string.order_card)
                else -> ""
            }
        }
    }

    inner class ViewHolder(itemView: View, adapter: OrdersAdapter) :
        RecyclerView.ViewHolder(itemView) {
        val tvValue = itemView.tvValue
        val tvEmail = itemView.tvEmail
        val tvDate = itemView.tvDate
        val tvOwnId = itemView.tvOwnId
        val ivIcon = itemView.ivIcon
        val tvStatus = itemView.tvStatus

        init {
            itemView.setOnClickListener {
                adapter.onItemHolderClick(this@ViewHolder)
            }
        }
    }
}