package br.com.wirecard.challenge.features.orders

interface OrdersActivityPresenter {
    fun logout()
}