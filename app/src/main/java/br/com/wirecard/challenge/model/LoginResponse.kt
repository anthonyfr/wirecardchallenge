package br.com.wirecard.challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LoginResponse(@SerializedName("accessToken") val accessToken: String? = null,
                         @SerializedName("moipAccount") val moipAccount: LoginIdentification): Serializable
