package br.com.wirecard.challenge.features.orders.details

interface OrderDetailsView {
    fun showLoading()
    fun hideLoading()
    fun onError(throwable: Throwable?, orderId: String)
    fun redirectToLogin()
    fun setSummary(id: String, ownId: String, total: Int, paymentType: String)
    fun setCustomer(fullname: String, email: String)
    fun setStatus(created: String, updated: String, status: String)
    fun setFinance(liquid: Int, fees: Int, total: Int)
    fun setPayment(payments: Int)
}