package br.com.wirecard.challenge.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import br.com.wirecard.challenge.App
import br.com.wirecard.challenge.R
import br.com.wirecard.challenge.features.login.LoginActivity
import br.com.wirecard.challenge.features.orders.OrdersActivity
import br.com.wirecard.challenge.features.orders.di.OrdersActivityModule
import br.com.wirecard.challenge.main.di.DaggerMainComponent
import br.com.wirecard.challenge.main.di.MainModule
import br.com.wirecard.challenge.model.StoreModel
import javax.inject.Inject

class MainActivity : Activity() {

    @Inject
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DaggerMainComponent.builder()
            .appComponent(App.appComponent)
            .mainModule(MainModule())
            .build().inject(this)

        checkDataAndRedirect()
    }

    /**
     * Checks if there is login data stored and redirects to activity.
     */
    private fun checkDataAndRedirect() {
        this@MainActivity.startActivity(
            Intent(this@MainActivity,
                if (presenter.isDataStored()) {
                    OrdersActivity::class.java
                } else {
                    LoginActivity::class.java
                }
            )
        )
        finish()
    }
}
