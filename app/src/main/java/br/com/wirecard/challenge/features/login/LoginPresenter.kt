package br.com.wirecard.challenge.features.login

/**
 * Interface for login presenter
 */
interface LoginPresenter {
    fun onLoginDataChanged(username: String, password: String)
    fun onSubmitClicked(username: String, password: String)
    fun onDestroy()
}