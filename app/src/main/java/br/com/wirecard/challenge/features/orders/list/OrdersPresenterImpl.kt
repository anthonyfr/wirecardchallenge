package br.com.wirecard.challenge.features.orders.list

import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.network.Api
import rx.android.schedulers.AndroidSchedulers
import rx.internal.util.SubscriptionList
import rx.schedulers.Schedulers

/**
 * Orders class to implement business logic
 */
class OrdersPresenterImpl(private var view: OrdersView, private  var api: Api, private var store: StoreModel):
    OrdersPresenter {

    private var subscriptionList = SubscriptionList()

    /**
     * Loads the orders list
     */
    override fun loadOrders() {
        if (store.isLoginDataStored()) {
            val accessToken = store.getAccessToken()
            subscriptionList.add(api.getOrders(String.format("OAuth %s", accessToken))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe({ response ->
                    view.loadSummary(response.summary)
                    view.loadRecycler(response.orders)
                }, { throwable ->
                    view.onError(throwable)
                }))
        } else {
            view.redirectToLogin()
        }
    }

    /**
     * Unsubscribes the subscription list on destroying activity context to avoid
     * memory leak and crashes.
     */
    override fun onDestroy() {
        if (!subscriptionList.isUnsubscribed) {
            subscriptionList.unsubscribe()
        }
    }

    /**
     * When a system failure related to the server or an unauthorized request happens,
     * the session is cleared and user redirected to the login screen.
     */
    override fun onRequestFailure() {
        store.clearSessionData()
        view.redirectToLogin()
    }
}