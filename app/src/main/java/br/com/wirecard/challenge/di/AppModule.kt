package br.com.wirecard.challenge.di

import br.com.wirecard.challenge.model.StoreModel
import br.com.wirecard.challenge.model.StoreModelImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideStoreModel(): StoreModel {
        return StoreModelImpl()
    }
}
